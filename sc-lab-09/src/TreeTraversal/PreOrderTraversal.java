package TreeTraversal;

import java.util.ArrayList;
import java.util.Stack;

public class PreOrderTraversal implements Traversal {

	@Override
	public ArrayList<String> traverse(Node root) {
		ArrayList<String> prelist = new ArrayList<String>();
		if(root == null) {
			return prelist;
		}
		
		Stack<Node> stack = new Stack<Node>();
		stack.push(root);
		
		while(!stack.isEmpty()) {
			Node top = stack.pop();
			prelist.add(top.getNode());
			
			
			if(top.getLeftNode() != null) {
				stack.push(top.getLeftNode());
			}
			
			if(top.getRightNode() != null) {
				stack.push(top.getRightNode());
			}
		}
		
		return prelist;
	}

}
