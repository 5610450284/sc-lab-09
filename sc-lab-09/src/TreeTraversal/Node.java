package TreeTraversal;

public class Node {
	private String node;
	private Node subnodeleft;
	private Node subnoderight;
	
	public Node(String node,Node subnodeleft,Node subnoderight) {
		this.node = node;
		this.subnodeleft = subnodeleft;
		this.subnoderight = subnoderight;
	}
	public String getNode() {
		return node;
	}
	public Node getLeftNode() {
		return subnodeleft;
	}
	public Node getRightNode() {
		return subnoderight;
	}

}
