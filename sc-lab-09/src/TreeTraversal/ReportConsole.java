package TreeTraversal;

import java.util.ArrayList;

public class ReportConsole {
	
	public ArrayList<String> display(Node root,Traversal traversal) {
		return traversal.traverse(root);
		
	}

}
