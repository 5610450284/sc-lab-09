package ComparableComparator;

import java.util.Comparator;

public class EarningComparator implements Comparator<Company> {

	@Override
	public int compare(Company o1, Company o2) {
		return (int) (o1.getIn() - o2.getIn());
	}

}
