package ComparableComparator;

import java.util.Comparator;

public class TaxableComparator implements Comparator<Taxable>{

	@Override
	public int compare(Taxable o1, Taxable o2) {
		// TODO Auto-generated method stub
		return (int)(o1.getTax() - o2.getTax());
	}

}
