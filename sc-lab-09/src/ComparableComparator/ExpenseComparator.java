package ComparableComparator;

import java.util.Comparator;

public class ExpenseComparator implements Comparator<Company> {

	@Override
	public int compare(Company o1, Company o2) {
		return (int) ((o1.getOut() - o2.getOut()));
	}

}
