package ComparableComparator;

import java.util.ArrayList;

public class TaxCalculator {
	public static double sum (ArrayList<Taxable> taxList){
		double sum=0;
		for(int i=0;i<taxList.size();i++){
			sum = sum+taxList.get(i).getTax();
		}
		return sum;
	}
}
