package ComparableComparator;

public interface Taxable {
	double getTax();

	String getName();
}
