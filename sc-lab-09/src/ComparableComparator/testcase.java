package ComparableComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class testcase {
	public static void main(String[] args){
		List<Person> lis1 = new ArrayList<Person>();
		List<Company> lis2 = new ArrayList<Company>();
		List<Product> lis3 = new ArrayList<Product>();
		List<Taxable> lis4 = new ArrayList<Taxable>();
		
		Person p = new Person("P1",500000);
		Person p2 = new Person("P2",300000);
		Person p3 = new Person("P3",100000);
		System.out.println("Compare :");
		if (p.compareTo(p2) > 0) {
			System.out.println(p.getName()+" is over than "+p2.getName()+" "+p.compareTo(p2));
		}
		else if (p.compareTo(p2) < 0) {
			System.out.println(p.getName()+" is less than "+p2.getName()+" "+(int)Math.abs(p.compareTo(p2)));
		}
		
		if (p3.compareTo(p2) > 0) {
			System.out.println(p3.getName()+" is over than "+p2.getName()+" "+p3.compareTo(p2));
		}
		else if (p3.compareTo(p2) < 0) {
			System.out.println(p3.getName()+" is less than "+p2.getName()+" "+(int)Math.abs(p3.compareTo(p2)));
		}
		lis1.add(p);
		lis1.add(p2);
		lis1.add(p3);
		System.out.println("---------------------------");
		System.out.println("Sort by Value");
		Collections.sort(lis1);
		for (int i =0;i < lis1.size();i++) {
			System.out.println(lis1.get(i).getName()+" : "+lis1.get(i).getValue());
		}
		
		System.out.println("---------------------------");
		
		Company c = new Company("C1", 1000000,200000);
		Company c2 = new Company("C2", 900000,700000);
		Company c3 = new Company("C3", 500000,100000);
		
		EarningComparator enc = new EarningComparator();
		ExpenseComparator exc = new ExpenseComparator();
		ProfitComparator pro = new ProfitComparator();
		System.out.println("Earning compare : ");
		//Earning
		if (enc.compare(c, c2) > 0) {
			System.out.println(c.getName()+ " earning over than "+c2.getName()+" "+enc.compare(c, c2));
		}
		else if (enc.compare(c, c2) < 0) {
			System.out.println(c.getName()+ " earning less than "+c2.getName()+" "+(int)Math.abs(enc.compare(c, c2)));
		}
		if (enc.compare(c, c3) > 0) {
			System.out.println(c.getName()+ " earning over than "+c3.getName()+" "+enc.compare(c, c3));
		}
		else if (enc.compare(c, c3) < 0) {
			System.out.println(c.getName()+ " earning less than "+c3.getName()+" "+(int)Math.abs(enc.compare(c, c3)));
		}
		System.out.println("---------------------------");
		System.out.println("Expense compare : ");
		//Expense
		if (exc.compare(c, c2) > 0) {
			System.out.println(c.getName()+ " expense over than "+c2.getName()+" "+exc.compare(c, c2));
		}
		else if (exc.compare(c, c2) < 0) {
			System.out.println(c.getName()+ " expense less than "+c2.getName()+" "+(int)Math.abs(exc.compare(c, c2)));
		}
		if (exc.compare(c, c3) > 0) {
			System.out.println(c.getName()+ " expense over than "+c3.getName()+" "+exc.compare(c, c3));
		}
		else if (exc.compare(c, c3) < 0) {
			System.out.println(c.getName()+ " expense less than "+c3.getName()+" "+(int)Math.abs(exc.compare(c, c3)));
		}
		System.out.println("---------------------------");
		System.out.println("Profit compare : ");
		//Profit
		if (pro.compare(c, c2) > 0) {
			System.out.println(c.getName()+ " profit over than "+c2.getName()+" "+pro.compare(c, c2));
		}
		else if (pro.compare(c, c2) < 0) {
			System.out.println(c.getName()+ " profit less than "+c2.getName()+" "+(int)Math.abs(pro.compare(c, c2)));
		}
		if (pro.compare(c, c3) > 0) {
			System.out.println(c.getName()+ " profit over than "+c3.getName()+" "+pro.compare(c, c3));
		}
		else if (pro.compare(c, c3) < 0) {
			System.out.println(c.getName()+ " profit less than "+c3.getName()+" "+(int)Math.abs(pro.compare(c, c3)));
		}
		System.out.println("---------------------------");
	
		lis2.add(c);
		lis2.add(c2);
		lis2.add(c3);
		System.out.println("Sort by Earning");
		Collections.sort(lis2,enc);
		for (int i =0;i < lis2.size();i++) {
			System.out.println(lis2.get(i).getName()+" : "+lis2.get(i).getIn());
		}
		System.out.println("---------------------------");
		System.out.println("Sort by Expense");
		Collections.sort(lis2,exc);
		for (int i =0;i < lis2.size();i++) {
			System.out.println(lis2.get(i).getName()+" : "+lis2.get(i).getOut());
		}
		System.out.println("---------------------------");
		System.out.println("Sort by Profit");
		Collections.sort(lis2,pro);
		for (int i =0;i < lis2.size();i++) {
			System.out.println(lis2.get(i).getName()+" : "+lis2.get(i).getProfit());
		}
		System.out.println("---------------------------");
		
		Product pd = new Product("pd1", 200);
		Product pd2 = new Product("pd2", 100);
		Product pd3 = new Product("pd3", 300);
		System.out.println("Compare : ");
		if (pd.compareTo(pd2) > 0) {
			System.out.println(pd.getName()+" is over than "+pd2.getName()+" "+pd.compareTo(pd2));
		}
		else if (pd.compareTo(pd2) < 0) {
			System.out.println(pd.getName()+" is less than "+pd2.getName()+" "+(int)Math.abs(pd.compareTo(pd2)));
		}
		
		if (pd3.compareTo(pd2) > 0) {
			System.out.println(pd3.getName()+" is over than "+pd2.getName()+" "+pd3.compareTo(pd2));
		}
		else if (pd3.compareTo(pd2) < 0) {
			System.out.println(pd3.getName()+" is less than "+pd2.getName()+" "+(int)Math.abs(pd3.compareTo(pd2)));
		}
		System.out.println("---------------------------");
		lis3.add(pd);
		lis3.add(pd2);
		lis3.add(pd3);
		System.out.println("Sort by Value");
		Collections.sort(lis3);
		for (int i =0;i < lis1.size();i++) {
			System.out.println(lis3.get(i).getName()+" : "+lis3.get(i).getValue());
		}
		System.out.println("---------------------------");
		//lis4
		lis4.add(p);
		lis4.add(p2);
		lis4.add(p3);
		lis4.add(c);
		lis4.add(c2);
		lis4.add(c3);
		lis4.add(pd);
		lis4.add(pd2);
		lis4.add(pd3);
		TaxableComparator txc = new TaxableComparator();
		Collections.sort(lis4,txc);
		for (int i =0;i < lis4.size();i++) {
			System.out.println((lis4.get(i)).getName()+" : "+lis4.get(i).getTax());
		}
		
	}
}
