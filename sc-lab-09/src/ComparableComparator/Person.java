package ComparableComparator;

public class Person implements Taxable,Comparable<Person> {
	String name;
	double value;
	public Person(String n,double v){
		name = n;
		value = v;
	}
	public double getValue() {
		return value;
	}
	
	public String getName() {
		return name;
	}

	@Override
	public double getTax() {
		double sum=0;
		if(value>300000){
			double tax1 = (300000*5)/100;
			double tax2 = ((value-300000)*10)/100;
			sum = tax1+tax2;
			}
		else{
			sum = (value*5)/100;
		}
		return sum;
	}

	@Override
	public int compareTo(Person o) {
		return (int) (this.value - o.getValue());
	}

}
