package ComparableComparator;

public class Product implements Taxable,Comparable<Product> {
	String name;
	double value;
	public Product(String n,double v){
		name = n;
		value = v;
		
	}
	public String getName() {
		return name;
	}
	public double getValue() {
		return value;
	}
	
	@Override
	public double getTax() {
		double ans = (value*7)/100;
		return ans;
	}
	@Override
	public int compareTo(Product o) {
		return (int) (this.value - o.getValue());
	}
	

}
