package ComparableComparator;

import java.util.Comparator;

public class ProfitComparator implements Comparator<Company> {

	@Override
	public int compare(Company o1, Company o2) {
		return (int) ((o1.getProfit() - o2.getProfit()));
	}

}
